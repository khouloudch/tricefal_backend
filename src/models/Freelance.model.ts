export class FreelanceModel{
    username:	string;
    contact:	ContactModel;
    company:	CompanyModel;
    privacyDetail:	PrivacyDetailModel;
    state:	FreelanceStateModel;
    }
    export class CompanyModel{
    raisonSocial:	string;
    nomCommercial:	string;
    formeJuridique:	string;
    capital:	string;
    rcs:	string;
    siret:	string;
    numDuns:	string;
    numTva:	string;
    codeNaf:	string;
    appartenanceGroupe:	string;
    typeEntreprise:	string;
    adminContact:	ContactModel;
    bankInfo:	BankInfoModel;
    fiscalAddress:	AddressModel;
    }

export class ContactModel{
    email:	string;
    lastName:	string;
    firstName:	string;
    langKey:	string;
    phone:	string;
    landline:	string;
    fax:	string;
    email2:	string;
    address:	AddressModel;
    }

export class BankInfoModel{
      iban:	string;
      owner:	string;
      bic:	string;
      address:	AddressModel;
      }

export class AddressModel{
        address:	string;
        addressMention:	string;
        postalCode:	string;
        city:	string;
        country:	string;
        }
export class PrivacyDetailModel{
      username:	string;
      birthDate:	string;
      birthCity:	string;
      birthCountry:	string;
      citizenship:	string;
      socialSecurityNumber:	string;
      nationalIdentityNumber:	string;
      information:	string;
      }

export class FreelanceStateModel{
        username:	string;
        kbisUploaded:	boolean;
        ribUploaded:	boolean;
        rcUploaded:	boolean;
        urssafUploaded:	boolean;
        fiscalUploaded:	boolean;
        completed:	boolean;
        }

