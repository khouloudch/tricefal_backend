import * as KeycloakConnect from "keycloak-connect";
import * as session from "express-session";
let keycloak: KeycloakConnect.Keycloak;

 function initKeycloak(): KeycloakConnect.Keycloak {
    if (keycloak) {
        console.warn("Trying to init Keycloak again!");
        return keycloak;
    } 
    else {
        console.log("Initializing Keycloak...");
        var memoryStore = new session.MemoryStore();
        keycloak = new KeycloakConnect({ store: memoryStore }, './keycloak.json');
        return keycloak;
    }
}

 function getKeycloak() {
    if (!keycloak){
        console.error('Keycloak has not been initialized. Please called init first.');
    } 
    return keycloak;
 }

module.exports = {
    initKeycloak,
    getKeycloak,
};
