import * as express from 'express';
import * as cors from 'cors';
import * as logger from 'morgan';
import * as bodyparser from 'body-parser';
import * as swaggerUi from 'swagger-ui-express';
import * as http from 'http';
import * as createError from 'http-errors';
import * as path from 'path';
import * as session from "express-session";
const clientCompanyRouter = require('./routes/client-company');
const keycloak_config = require('./config/config-keycloak');


const app = express();
app.use(cors());
app.use(bodyparser.json());

app.use(cors());
app.use(bodyparser.json());
app.use(bodyparser.urlencoded({ extended: true }));
require('dotenv').config();

app.use(logger('dev'));
app.use(cors())
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, 'public')));


app.all('/*', function(req, res, next) {

  res.header("Access-Control-Allow-Origin", "*"); 
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
  res.header('Access-Control-Allow-Headers', 'Content-type,Accept,X-Access-Token,X-Key');

  if (req.method == 'OPTIONS') {
      res.status(200).end();
  } else {
      next();
  }
});


const PORT = process.env.PORT
const server = http.createServer(app);
server.listen(PORT);
server.on('listening', async () => {
	console.info(`Listening on port ${PORT}`);
});

app.get( "/", ( req, res ) => {
	res.send( "Hello world!" );
  } );
  

// swagger
try {
	const swaggerDocument = require('../swagger.json');
	app.use('/docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));
} catch (err) {
	console.error('Unable to read swagger.json', err);
}


// keycloak
 const keycloak = keycloak_config.initKeycloak();

 app.use(session({
  secret:process.env.SECRET,
  resave: false,
  saveUninitialized: true,
  store: new session.MemoryStore(),
  cookie: { secure: true }
}))


app.use(keycloak.middleware());

//This APIs should be accessible to users with the role admin or user
app.get('/all', keycloak.protect(['user']), (req, response) => {
  console.log(response);
    response.send('heelo you')
});


var testController = require('./middleware/controller.ts');

app.use('/test', testController);
app.use('/api/user' ,keycloak.protect(['ac_freelance_w']), clientCompanyRouter)

// catch 404 and forward to error handler
app.use(function(req, res, next) {
	next(createError(404));
  });
  
  // error handler
  app.use(function(err, req, res, next) {
	res.locals.message = err.message;
	res.locals.error = req.app.get('env') === 'development' ? err : {};
	res.status(err.status || 500);
  });
  

  module.exports = { app };
